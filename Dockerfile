# Builder
FROM golang:1.21-alpine AS builder

WORKDIR /app

# Install dependencies
COPY go.mod go.sum ./
RUN go mod download

# Build the Go app
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -o /deploy-to-portainer .

# Final
FROM alpine:latest

WORKDIR /

# Copy the Pre-built binary file from the previous stage
COPY --from=builder /deploy-to-portainer /
