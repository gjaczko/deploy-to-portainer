package main

import (
	"fmt"
	"os"
	"time"
)

func main() {
	// save current time to measure execution time
	startTime := time.Now()

	// process command line arguments
	config := ParseConfig()

	// print configuration
	printIfVerbose(config.verbose, "Configuration parsed:")
	printIfVerbose(config.verbose, "Portainer deployment target: %s", config.target)
	printIfVerbose(config.verbose, "Portainer stack name: %s", config.stackName)
	printIfVerbose(config.verbose, "Git repository URL: %s", config.gitRepository)
	printIfVerbose(config.verbose, "Git branch: %s", config.gitBranch)
	printIfVerbose(config.verbose, "Docker compose file: %s", config.composeFile)
	printIfVerbose(config.verbose, "Environment file: %s", config.envFile)
	printIfVerbose(config.verbose, "Stack path: %s\n", config.stackPath)

	// create portainer API client
	client := NewPortainerClient(config)

	// get portainer deployment target
	target := client.GetTargetByName()
	if target == nil {
		println("Error: Portainer deployment target not found")
		os.Exit(1)
	}
	printIfVerbose(config.verbose, "Portainer deployment target found: %s (%d)", target.Name, target.Id)

	// get portainer stack
	stack := client.GetStackByTargetAndName(target.Id)
	if stack == nil {
		printIfVerbose(config.verbose, "Portainer stack not found, creating a new one")

		// create portainer stack
		stack = client.CreateStack(target.Id)
		if stack == nil {
			println("Error: Failed to create Portainer stack")
			os.Exit(1)
		}

		printIfVerbose(config.verbose, "Portainer stack created: %s (%d)", stack.Name, stack.Id)
	} else {
		printIfVerbose(config.verbose, "Portainer stack found: %s (%d), updating", stack.Name, stack.Id)

		// redeploy portainer stack
		stack = client.RedeployStack(target.Id, stack.Id)
		if stack == nil {
			println("Error: Failed to update Portainer stack")
			os.Exit(1)
		}

		printIfVerbose(config.verbose, "Portainer stack updated: %s (%d)", stack.Name, stack.Id)
	}

	// print execution time including milliseconds
	printIfVerbose(config.verbose, "Execution time: %s", time.Since(startTime).Round(time.Millisecond))
}

func printIfVerbose(verbose bool, format string, a ...interface{}) {
	if verbose {
		fmt.Printf(format+"\n", a...)
	}
}
