package main

import (
	"flag"
	"fmt"
	"os"
	"regexp"
	"strings"
)

type Config struct {
	verbose       bool
	apiUrl        string
	apiToken      string
	target        string
	stackName     string
	gitRepository string
	gitBranch     string
	gitUsername   string
	gitPassword   string
	composeFile   string
	envFile       string
	relativePaths bool
	stackPath     string
}

// ParseConfig parses command line arguments and returns a Config struct
func ParseConfig() Config {
	// define default configuration
	var config = Config{
		target:      "local",
		gitBranch:   "master",
		composeFile: "docker-compose.yml",
		stackPath:   "/tmp",
	}

	// define flags
	showHelp := flag.Bool("help", false, "Show this help message and exit")
	flag.BoolVar(&config.verbose, "verbose", config.verbose, "Enable verbose logging")
	flag.BoolVar(&config.verbose, "v", config.verbose, "Enable verbose logging")
	flag.StringVar(&config.target, "target", config.target, "Portainer deployment target")
	flag.StringVar(&config.target, "t", config.target, "Portainer deployment target")
	flag.StringVar(&config.gitBranch, "branch", config.gitBranch, "Git branch")
	flag.StringVar(&config.gitBranch, "b", config.gitBranch, "Git branch")
	flag.StringVar(&config.gitUsername, "username", config.gitUsername, "Git username")
	flag.StringVar(&config.gitUsername, "u", config.gitUsername, "Git username")
	flag.StringVar(&config.gitPassword, "password", config.gitPassword, "Git password")
	flag.StringVar(&config.gitPassword, "p", config.gitPassword, "Git password")
	flag.StringVar(&config.composeFile, "compose-file", config.composeFile, "Docker compose file")
	flag.StringVar(&config.composeFile, "c", config.composeFile, "Docker compose file")
	flag.StringVar(&config.envFile, "env-file", config.envFile, "Environment file")
	flag.StringVar(&config.envFile, "e", config.envFile, "Environment file")
	flag.BoolVar(&config.relativePaths, "enable-relative-paths", config.relativePaths, "Enable relative path volumes")
	flag.StringVar(&config.stackPath, "stack-path", config.stackPath, "Filesystem path for the stack")

	// custom usage message
	flag.Usage = func() {
		_, _ = fmt.Fprintf(flag.CommandLine.Output(), "Deploy stack using Portainer API.\n\n")
		_, _ = fmt.Fprintf(flag.CommandLine.Output(), "Usage:\n")
		_, _ = fmt.Fprintf(flag.CommandLine.Output(), "  %s --help\n", os.Args[0])
		_, _ = fmt.Fprintf(flag.CommandLine.Output(), "  %s [options] stack-name git-repository\n\n", os.Args[0])
		_, _ = fmt.Fprintf(flag.CommandLine.Output(), "Arguments:\n")
		_, _ = fmt.Fprintf(flag.CommandLine.Output(), "  stack-name\n\tPortainer stack name\n")
		_, _ = fmt.Fprintf(flag.CommandLine.Output(), "  git-repository\n\tGit repository URL\n\n")
		_, _ = fmt.Fprintf(flag.CommandLine.Output(), "Options:\n")
		flag.PrintDefaults()
	}

	// parse flags
	flag.Parse()

	// print help message and exit if no arguments are provided
	if *showHelp || flag.NArg() == 0 {
		flag.Usage()
		os.Exit(0)
	}

	// save positional arguments
	if flag.NArg() != 2 {
		println("Error: Invalid number of arguments")
		flag.Usage()
		os.Exit(1)
	}
	config.stackName = flag.Arg(0)
	config.gitRepository = flag.Arg(1)

	// get git credentials from repository URL
	if config.gitUsername == "" && config.gitPassword == "" {
		matches := regexp.MustCompile("^(https?://)(.*):(.*)@(.*)$").
			FindStringSubmatch(config.gitRepository)

		if len(matches) == 5 {
			config.gitRepository = matches[1] + matches[4]
			config.gitUsername = matches[2]
			config.gitPassword = matches[3]
		}
	}

	// read api credentials from environment variables
	if os.Getenv("PORTAINER_URL") == "" || os.Getenv("PORTAINER_TOKEN") == "" {
		println("Error: Portainer API credentials not found")
		os.Exit(1)
	}
	config.apiUrl = strings.TrimRight(os.Getenv("PORTAINER_URL"), "/") + "/api"
	config.apiToken = os.Getenv("PORTAINER_TOKEN")

	return config
}

// ReadEnvFile reads an environment file and returns the content as an array of name-value pairs
func ReadEnvFile(envFile string) []map[string]interface{} {
	envFileContent, err := os.ReadFile(envFile)
	if err != nil {
		println("Error: " + err.Error())
		os.Exit(1)
	}

	env := make([]map[string]interface{}, 0)

	for _, envFileLine := range strings.Split(string(envFileContent), "\n") {
		if envFileLine == "" || strings.HasPrefix(envFileLine, "#") {
			continue
		}

		envFileLineParts := strings.SplitN(envFileLine, "=", 2)
		if len(envFileLineParts) != 2 {
			println("Warning: Invalid line in environment file: " + envFileLine)
			continue
		}

		env = append(env, map[string]interface{}{
			"name":  envFileLineParts[0],
			"value": strings.Trim(envFileLineParts[1], "\""),
		})
	}

	if len(env) == 0 {
		return nil
	}

	return env
}
