# deploy-to-portainer

Docker image for deploying Portainer stacks from git repositories, designed for use in Gitlab CI/CD pipelines.

## CLI parameters
```bash
deploy-to-portainer [options] <stack-name> <git-repo-url>

Arguments:
  stack-name                Name of the stack to deploy
  git-repo-url              URL of the git repository to deploy from

Options:
  --target <target>         Target Portainer endpoint (default: "local")
  --branch <branch>         Git branch to deploy from (default: "master")
  --compose-file <file>     Docker Compose file to deploy (default: "docker-compose.yml")
  --env-file <file>         Environment file to use
  --username <username>     Username to use for git authentication
  --password <password>     Password to use for git authentication
  --enable-relative-paths   Enable relative paths in docker-compose file
  --stack-path <path>       Filesystem path for the stack (default: "/tmp")
  --verbose                 Show verbose output
  --help                    Show help
```

## Example usage in Gitlab CI/CD pipeline
```yaml
# File: .gitlab-ci.yml

stages:
  - [...]
  - deploy

[...]

deploy:
  stage: deploy
  image: registry.gitlab.com/gjaczko/deploy-to-portainer:latest
  variables:
    PORTAINER_URL: $PORTAINER_URL
    PORTAINER_TOKEN: $PORTAINER_TOKEN
  script:
    - /deploy-to-portainer --compose-file docker-compose.production.yaml --env-file .env my-stack https://gitlab.example.com/my-stack.git
```
