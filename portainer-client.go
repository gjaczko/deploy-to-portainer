package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gopkg.in/yaml.v3"
	"io"
	"net/http"
	"os"
)

type PortainerClient struct {
	config     Config
	httpClient *http.Client
}

type PortainerTarget struct {
	Id   int    `json:"Id"`
	Name string `json:"Name"`
}

type PortainerStack struct {
	Id       int    `json:"Id"`
	Name     string `json:"Name"`
	TargetId int    `json:"EndpointId"`
}

// NewPortainerClient creates a new Portainer API client
func NewPortainerClient(c Config) *PortainerClient {
	return &PortainerClient{
		config:     c,
		httpClient: &http.Client{},
	}
}

// GetTargetByName returns a Portainer deployment target by name
func (pc *PortainerClient) GetTargetByName() *PortainerTarget {

	query := "/endpoints?search=" + pc.config.target

	response, err := pc.sendRequest("GET", query)
	if err != nil {
		println("Error: " + err.Error())
		return nil
	}

	var targets []PortainerTarget
	err = json.Unmarshal([]byte(response), &targets)
	if err != nil {
		println("Error: " + err.Error())
		return nil
	}

	if len(targets) == 0 {
		return nil
	}

	if len(targets) > 1 {
		println("Error: Multiple Portainer deployment targets found")
		return nil
	}

	return &targets[0]
}

// GetStackByTargetAndName returns a Portainer stack by target ID and stack name
func (pc *PortainerClient) GetStackByTargetAndName(targetId int) *PortainerStack {
	response, err := pc.sendRequest("GET", "/stacks")
	if err != nil {
		println("Error: " + err.Error())
		return nil
	}

	var stacks []PortainerStack
	err = json.Unmarshal([]byte(response), &stacks)
	if err != nil {
		println("Error: " + err.Error())
		return nil
	}

	for _, stack := range stacks {
		if stack.TargetId == targetId && stack.Name == pc.config.stackName {
			return &stack
		}
	}

	return nil
}

// CreateStack creates a new Portainer stack
func (pc *PortainerClient) CreateStack(targetId int) *PortainerStack {
	method := "POST"
	// create request path
	path := "/stacks/create/standalone/repository?endpointId=" + fmt.Sprintf("%d", targetId)

	// create request body
	body := map[string]interface{}{
		"name":                    pc.config.stackName,
		"repositoryURL":           pc.config.gitRepository,
		"repositoryReferenceName": fmt.Sprintf("refs/heads/%s", pc.config.gitBranch),
		"composeFile":             pc.config.composeFile,
		"supportRelativePath":     false,
	}

	// check if relative path is enabled
	if pc.config.relativePaths {
		// check if we have any relative paths in the compose file
		if pc.hasRelativePaths(pc.config.composeFile) {
			// enable relative path
			body["supportRelativePath"] = true
			body["filesystemPath"] = pc.config.stackPath
		}
	}

	return pc._deployStack(method, path, body)
}

// RedeployStack redeploys a Portainer stack
func (pc *PortainerClient) RedeployStack(targetId int, stackId int) *PortainerStack {
	method := "PUT"
	// create request path
	path := fmt.Sprintf("/stacks/%d/git/redeploy?endpointId=%d", stackId, targetId)

	// create request body
	body := map[string]interface{}{
		"prune":                   true,
		"pullImage":               true,
		"repositoryReferenceName": fmt.Sprintf("refs/heads/%s", pc.config.gitBranch),
	}

	return pc._deployStack(method, path, body)
}

func (pc *PortainerClient) _deployStack(method string, path string, body map[string]interface{}) *PortainerStack {
	// add git credentials to request body
	if pc.config.gitPassword != "" {
		body["repositoryAuthentication"] = true
		body["repositoryPassword"] = pc.config.gitPassword
		if pc.config.gitUsername != "" {
			body["repositoryUsername"] = pc.config.gitUsername
		} else {
			body["repositoryUsername"] = "git"
		}
	}

	// add environment variables to request body
	if pc.config.envFile != "" {
		if env := ReadEnvFile(pc.config.envFile); env != nil {
			body["env"] = env
		}
	}

	// convert request body to JSON
	jsonBody, err := json.Marshal(body)
	if err != nil {
		println("Error: " + err.Error())
		return nil
	}

	//// dump request
	//fmt.Printf("%s %s\n%s\n", method, path, jsonBody)

	// send request
	response, err := pc.sendRequestWithBody(method, path, bytes.NewBuffer(jsonBody))
	if err != nil {
		println("Error: " + err.Error())
		return nil
	}

	// parse response
	var stack PortainerStack
	err = json.Unmarshal([]byte(response), &stack)
	if err != nil {
		println("Error: " + err.Error())
		return nil
	}

	return &stack
}

// sendRequest sends a request to the Portainer API and returns the response body as a string
func (pc *PortainerClient) sendRequest(method string, path string) (string, error) {
	return pc.sendRequestWithBody(method, path, nil)
}

// sendRequestWithBody sends a request to the Portainer API and returns the response body as a string
func (pc *PortainerClient) sendRequestWithBody(method string, path string, body io.Reader) (string, error) {
	url := pc.config.apiUrl + path
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return "", err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-API-Key", pc.config.apiToken)

	resp, err := pc.httpClient.Do(req)
	if err != nil {
		return "", err
	}
	defer func(Body io.ReadCloser) {
		_ = Body.Close()
	}(resp.Body)

	responseBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	if resp.StatusCode != 200 {
		return "", fmt.Errorf("HTTP error: %s (%s)", resp.Status, responseBody)
	}

	return string(responseBody), nil
}

// hasRelativePaths checks if a compose file has any relative paths
func (pc *PortainerClient) hasRelativePaths(composeFile string) bool {
	// read compose file
	composeFileContent, err := os.ReadFile(composeFile)
	if err != nil {
		println("Error: " + err.Error())
		return false
	}

	// parse compose file
	var composeFileData map[string]interface{}
	err = yaml.Unmarshal(composeFileContent, &composeFileData)
	if err != nil {
		println("Error: " + err.Error())
		return false
	}

	// check if we have any relative paths
	for _, service := range composeFileData["services"].(map[string]interface{}) {
		if service.(map[string]interface{})["volumes"] != nil {
			for _, volume := range service.(map[string]interface{})["volumes"].([]interface{}) {
				if volume.(string)[0] == '.' {
					return true
				}
			}
		}
	}

	return false
}
